create database final;

use final;

Create table vehiculos(
id int auto_increment primary key , 
nombre varchar(50),
precio REAL,matricula varchar(50) not null UNIQUE);
 
Create table pilotos(
id int auto_increment primary key,
nombre varchar(50),
trofeos int,
proxima_carrera date,
vehiculo int,
FOREIGN KEY(vehiculo) references vehiculos(id) ON DELETE set null
);
Create table mecanicos(
id int auto_increment primary key,
nombre varchar(50),
añosExperiencia int,
formacion varchar(50),
vehiculo int,
FOREIGN KEY(vehiculo) references vehiculos(id) ON DELETE set null
);

Create table circuitos(id int auto_increment primary key,
nombre varchar(50) UNIQUE NOT NULL,
localidad varchar(50),
cantidadPistas int
);

create table pilotos_circuito(
id_piloto int not null references pilotos(id),
id_circuito int  not null  references circuitos(id),
primary key(id_piloto,id_circuito)
 );
 
 


