package usuarios;
/**
 * Created by DAM on 23/10/2018.
 * clases comentada
 */
public class Usuario {
	
	private String nombre;
	private String contrasena;
	private double saldo;


	
	public Usuario(){
		
	}

	/**
	 * @apiNote creo el constructor para inicializar los atributos de la clases
	 *
	 * @param nombre
	 * @param contrasena
	 * @param saldo
	 */
	public Usuario(String nombre,String contrasena,double saldo){
		this.nombre=nombre;
		this.contrasena=contrasena;

		this.saldo=saldo;
		
	}

    /**
     *
     * @apiNote constructor para incializar el objeto con nombre y contraseña
     *
     * @param nombre
     * @param contrasena
     */
	public Usuario(String nombre,String contrasena){
		this.nombre=nombre;
		this.contrasena=contrasena;

		
	}



	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	
	

}
