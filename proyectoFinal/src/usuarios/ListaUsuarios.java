package usuarios;

import java.util.ArrayList;


/**
 * Created by DAM on 23/10/2018.
 * clases comentada
 */
public class ListaUsuarios {
	
	private ArrayList<Usuario> listaUsuario;
	//constructor para inicializar la lista de usuarios
	public ListaUsuarios() {
		listaUsuario = new ArrayList<Usuario>();
		//metodo para añadir usuarios al array
		anadirUsuarios();
	}
    //Anadimos 2 usuarios para realizar las pruebas
	private void anadirUsuarios() {
		Usuario normal = new Usuario("juan","1234",40);
		Usuario admin = new Usuario("admin","1234",50);
		
		listaUsuario.add(admin);
		listaUsuario.add(normal);
	}
//getter y setter de el array de usuarios
	public ArrayList<Usuario> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(ArrayList<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}
	
	public void añadirUsuario(String nombre,String contraseña){
		Usuario newUsuario = new Usuario(nombre,contraseña);

		listaUsuario.add(newUsuario);
	}
	

}
