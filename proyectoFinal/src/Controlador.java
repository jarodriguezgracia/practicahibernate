import base.Circuitos;
import base.Mecanicos;
import base.Pilotos;
import base.Vehiculos;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.hibernate.LazyInitializationException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Controlador implements ActionListener, ListSelectionListener {

    Vista vista;
    Modelo modelo;
    HiloUpdate hilo;
    VistaAccesso vistaAcesso;

    public Controlador(Vista unavista, Modelo unModelo, VistaAccesso vistaAcceso) {
        this.vista=unavista;
        this.modelo=unModelo;
        this.vistaAcesso=vistaAcceso;


        modelo.conectar();

        añadirActionListener(this);
        añadirListSelectionListener(this);

        actualizar(vista.dlmMecanicos, "mecanicos");


        actualizar(vista.listaVehiculos, "vehiculos");

        actualizarComboxVehiculo(vista.listaVehiculosEnPilotos,"vehiculosPilotos");
        actualizarComboxVehiculo(vista.listaVehiculosEnMecanicos,"vehiculosMecanicos");

        actualizar(vista.dlmPilotos, "pilotos");
        actualizar(vista.dlmCircuitos, "circuitos");
        actualizar(vista.dlmCircutiosEnlace, "circuitosEnlace");
        //pilotosenlace
        actualizar(vista.dlmPilotosEnlace, "pilotos");
        hilo = new HiloUpdate(vista,modelo,this);
        hilo.start();
    }



    private void añadirListSelectionListener(ListSelectionListener listener) {
        vista.listCircuitosEnlace.addListSelectionListener(listener);
        vista.listCircuitos.addListSelectionListener(listener);
        vista.listPilotos.addListSelectionListener(listener);
        vista.listVehiculos.addListSelectionListener(listener);
        vista.listMecanicos.addListSelectionListener(listener);
        vista.listCircuitos.addListSelectionListener(listener);

    }


    private void añadirActionListener(Controlador listener) {

        //implemento los listener a los botones
        this.vista.ButtonAltaPiloto.addActionListener(listener);
        this.vista.ButtonEliminarPiloto.addActionListener(listener);
        vista.btAltaCircuito.addActionListener(listener);
        vista.btnEliminarCircuito.addActionListener(listener);
        vista.añadirPilotosHaCircuitoButton.addActionListener(listener);

        this.vista.ButtonAltaVehiculo.addActionListener(listener);
        this.vista.ButtonEliminarVehiculo.addActionListener(listener);
        vista.comboBoxVehiculoPiloto.addActionListener(listener);

        vista.btModificarCircuito.addActionListener(listener);
        vista.buttonModificarPiloto.addActionListener(listener);
        vista.ButtonModificarVehiculo.addActionListener(listener);



        vista.AltaMecanico.addActionListener(listener);
        vista.EliminarMecanico.addActionListener(listener);
        vista.ModificarMecanico.addActionListener(listener);
        vista.eliminarPilotosDeCircuito.addActionListener(listener);
        vista.opcionSalir.addActionListener(listener);





    }

    @Override
    public void actionPerformed(ActionEvent e) {

        switch (e.getActionCommand()){

            case "Salir":
                vista.frame.setVisible(false);
                vistaAcesso.frame.setVisible(true);


                break;



            case "añadirPilotosCircuito":
                //(String nombre, float precio,String matricula,String tipo,String diseño)
                Pilotos piloto1 =(Pilotos) vista.listPilotosEnlace.getSelectedValue();
                Circuitos circuitos = (Circuitos) vista.listCircuitosEnlace.getSelectedValue();
                try {

                    modelo.insertarPilotosEnCircuito(piloto1, circuitos);
                }catch (Exception ee){
                    System.out.println("entro");
                    ee.printStackTrace();
                    vista.mensajeDeError();
                }
                System.out.println("paso");
                actualizar(vista.dlmPilotos, "pilotos");
                actualizar(vista.dlmCircuitos, "circuitos");
                actualizar(vista.dlmCircutiosEnlace, "circuitos");
               // actualizar(vista.dlmPilotosEnlace, "pilotos");

                //pilotosenlace
               // actualizar(vista.dlmPilotosEnlace, "pilotos");




                break;
            case "AltaVehiculo":
                //(String nombre, float precio,String matricula,String tipo,String diseño)


                    modelo.insertarVehiculo(vista.textNombreVehiculo.getText(), vista.textPrecioVehiculo.getText(),vista.txtMatriculaVehiculo.getText());

                actualizar(vista.listaVehiculos, "vehiculos");

               actualizarComboxVehiculo(vista.listaVehiculosEnPilotos,"vehiculosPilotos");
                actualizarComboxVehiculo(vista.listaVehiculosEnMecanicos,"vehiculosMecanicos");

                break;

            case "AltaMecanico":
                //(String nombre, float precio,String matricula,String tipo,String diseño)


                modelo.insertarMecanico(vista.textMecanicoNombre.getText(), vista.textMecanicosExperiencia.getText(),vista.textMecanicosFormacion.getText(), (Vehiculos) vista.listaVehiculosEnMecanicos.getSelectedItem());

                actualizar(vista.dlmMecanicos, "mecanicos");


                break;
            case "modificar":

                if (e.getSource() == vista.ModificarMecanico) {
                Mecanicos mecanico = (Mecanicos) vista.listMecanicos.getSelectedValue();
                    modificarMecanico(mecanico);
                    modelo.actualizarMecanico(mecanico);

                }else  if (e.getSource() == vista.ButtonModificarVehiculo) {
                    Vehiculos vehiculo = (Vehiculos) vista.listVehiculos.getSelectedValue();
                    modificarVehiculo(vehiculo);
                    modelo.actualizarVehiculo(vehiculo);

                }else  if (e.getSource() == vista.buttonModificarPiloto) {

                    Pilotos piloto = (Pilotos) vista.listPilotos.getSelectedValue();
                    modificarPiloto(piloto);
                    modelo.actualizarPiloto(piloto);
                    //pilotosenlace
                    actualizar(vista.dlmPilotosEnlace, "pilotos");

                }else  if (e.getSource() == vista.btModificarCircuito) {
                    Circuitos circuit = (Circuitos) vista.listCircuitos.getSelectedValue();
                    modificarCircuito(circuit);
                    modelo.actualizarCircuito(circuit);
                    //pilotosenlace
                    actualizar(vista.dlmPilotosEnlace, "pilotos");

                }


                break;
            case "AltaPiloto":
                //(String nombre, float precio,String matricula,String tipo,String diseño)

                modelo.insertarPilotos(vista.textNombrePiloto.getText(), Integer.parseInt(vista.textTrofeosPiloto.getText()),
                        vista.txtFechaPiloto.getDate(), (Vehiculos) vista.listaVehiculosEnPilotos.getSelectedItem());

                actualizar(vista.dlmPilotos, "pilotos");
               // actualizar(vista.dlmPilotosEnlace, "pilotosEnlace");
                //pilotosenlace
                actualizar(vista.dlmPilotosEnlace, "pilotos");


                break;

            case "altaCircuito":

                    modelo.insertarCircuitos(vista.txtNombreCircuito.getText(), vista.textLocalidadCircuito.getText(),vista.textFieldPistas.getText());

                actualizar(vista.dlmCircuitos, "circuitos");
                actualizar(vista.dlmCircutiosEnlace, "circuitosEnlace");

                //actualizarJlistCircuitos(vista.dlmCircuitos, "circuitos");
                break;
            case "eliminar":
                if (e.getSource() == vista.ButtonEliminarVehiculo) {

                    Vehiculos vehiculo =(Vehiculos) vista.listVehiculos.getSelectedValue();
                    if(vehiculo!=null) {
                        for (int i = 0; i < vehiculo.getMecanicos().size(); i++) {
                            if (vehiculo.getMecanicos().get(i).getVehiculo().equals(vehiculo)) {
                                vehiculo.getMecanicos().get(i).setVehiculo(null);
                            }
                        }
                        for (int i = 0; i < vehiculo.getPilotos().size(); i++) {
                            if (vehiculo.getPilotos().get(i).getVehiculo().equals(vehiculo)) {
                                vehiculo.getPilotos().get(i).setVehiculo(null);
                            }
                        }
                        modelo.eliminarVehiculo(vehiculo);
                    }else {
                        vista.mensajeNoHaySelecion();
                    }

                } else if (e.getSource() == vista.ButtonEliminarPiloto) {

                    try {
                        Pilotos delPiloto = (Pilotos) vista.listPilotos.getSelectedValue();

                        if(delPiloto!=null) {
                            modelo.eliminarPiloto(delPiloto);
                        }else {
                            vista.mensajeNoHaySelecion();
                        }
                    }catch (javax.persistence.PersistenceException eue){
                        eue.printStackTrace();
                        vista.mensajeDerelacion();
                    }catch (Exception eu){
                        eu.printStackTrace();

                    }

                } else if (e.getSource() == vista.btnEliminarCircuito) {

                    Circuitos delCircuitos= (Circuitos) vista.listCircuitos.getSelectedValue();

                    if(delCircuitos!=null) {
                        modelo.eliminarCircuito(delCircuitos);
                    }else{
                        vista.mensajeNoHaySelecion();
                    }


                }else if (e.getSource() == vista.EliminarMecanico) {


                    Mecanicos mecanicoDel = (Mecanicos) vista.listMecanicos.getSelectedValue();

                    if(mecanicoDel!=null) {
                        modelo.eliminarMecanico(mecanicoDel);
                    }else{
                        vista.mensajeNoHaySelecion();
                    }

                }else if(e.getSource() == vista.eliminarPilotosDeCircuito){

                    Circuitos circuito = (Circuitos) vista.listCircuitos.getSelectedValue();
                    Pilotos piloto = (Pilotos) vista.listPilotosCircuitos.getSelectedValue();
                    if (circuito!=null && piloto!=null){
                        modelo.eliminarRelacion(circuito,piloto);

                        vista.dlmListPilotosCircuitos.removeAllElements();
                        System.out.println(circuito.getPilotos().size());
                        for (Pilotos pilo:circuito.getPilotos()) {
                            vista.dlmListPilotosCircuitos.addElement(pilo);
                        }

                    }else{
                        vista.mensajeSelecionCircuitoPiloto();
                    }

                }

                actualizar(vista.dlmMecanicos, "mecanicos");


                actualizar(vista.listaVehiculos, "vehiculos");

                actualizarComboxVehiculo(vista.listaVehiculosEnPilotos,"vehiculosPilotos");
                actualizarComboxVehiculo(vista.listaVehiculosEnMecanicos,"vehiculosMecanicos");

                actualizar(vista.dlmPilotos, "pilotos");
                actualizar(vista.dlmCircuitos, "circuitos");
                actualizar(vista.dlmCircutiosEnlace, "circuitos");
                actualizar(vista.dlmPilotosEnlace, "pilotos");
                break;

        }

    }

    private void modificarCircuito(Circuitos circuit) {
        circuit.setNombre(vista.txtNombreCircuito.getText());
        circuit.setCantidadPistas(Integer.parseInt(vista.textFieldPistas.getText()));
        circuit.setLocalidad(vista.textLocalidadCircuito.getText());
    }

    private void modificarVehiculo(Vehiculos vehiculo) {
        vehiculo.setNombre(vista.textNombreVehiculo.getText());
        vehiculo.setMatricula(vista.txtMatriculaVehiculo.getText());
        vehiculo.setPrecio(Double.parseDouble(vista.textPrecioVehiculo.getText()));
    }

    public void actualizarComboxVehiculo(DefaultComboBoxModel dlmComboBox, String tipo) {
        dlmComboBox.removeAllElements();

        if (tipo.equals("vehiculosPilotos")) {
            List<Vehiculos> lista = modelo.getVehiculos();

            for (Vehiculos vehiculo : lista) {
                dlmComboBox.addElement(vehiculo);
            }
        }
        if (tipo.equals("vehiculosMecanicos")) {
            List<Vehiculos> lista = modelo.getVehiculos();

            for (Vehiculos vehiculo : lista) {
                dlmComboBox.addElement(vehiculo);
            }
        }
    }

    public void actualizar(DefaultListModel listaGeneral, String tipo) {
    listaGeneral.removeAllElements();
        if(tipo.equals("vehiculos")){

            List<Vehiculos> lista = modelo.getVehiculos();
            for (Vehiculos dc:lista) {
                listaGeneral.addElement(dc);
            }

        }else if(tipo.equals("mecanicos")){
            List<Mecanicos> lista = modelo.getMecanicos();
            for (Mecanicos dc:lista) {
                listaGeneral.addElement(dc);
            }

        }else if(tipo.equals("pilotos")){
            List<Pilotos> lista = modelo.getPilotos();
            for (Pilotos dc:lista) {
                listaGeneral.addElement(dc);
            }

        }else if(tipo.equals("circuitos")){
            List<Circuitos> lista = modelo.getCircuitos();
            for (Circuitos dc:lista) {
                listaGeneral.addElement(dc);
            }

        }
        /**
        else if(tipo.equals("pilotosEnlace")){
            Circuitos circuito = (Circuitos) vista.listCircuitosEnlace.getSelectedValue();
            List<Pilotos> lista = modelo.getPilotosCircuito(circuito);
            for (Pilotos dc:lista) {
                listaGeneral.addElement(dc);
            }

        }
         **/
         else if(tipo.equals("circuitosEnlace")){
            List<Circuitos> lista = modelo.getCircuitos();
            for (Circuitos dc:lista) {
                listaGeneral.addElement(dc);
            }

        }





    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
/**
        if(e.getSource().equals(vista.listCircuitosEnlace)){

            Circuitos circuito = (Circuitos) vista.listCircuitosEnlace.getSelectedValue();



            List<Pilotos> todospilotos = modelo.getPilotos();

            List<Pilotos> PilotosFueraDeCircuito = new ArrayList<>();

            for (Pilotos piloto:todospilotos) {
                int cont = 0;

                    for (Circuitos circ : piloto.getCircuitos()) {
                        if (circ.equals(circuito)) {
                            System.out.println("sumo contador");
                            cont++;
                        }
                    }
                    if (cont == 0) {
                        System.out.println("añado piloto");
                        PilotosFueraDeCircuito.add(piloto);
                    }

            }

            vista.dlmPilotosEnlace.removeAllElements();
            for (Pilotos piloto:PilotosFueraDeCircuito) {
                vista.dlmPilotosEnlace.addElement(piloto);
            }
            //actualizar(vista.dlmListPilotosCircuitos,"pilotosEnlace");
        /**

            List<Pilotos> outPilotos = new ArrayList<>();
            for (int i = 0; i <allPilotos.size()-1 ; i++) {
                int cont=0;


                for (int j = 0; j <allPilotos.get(i).getCircuitos().size()-1 ; j++) {

                    if (allPilotos.get(i).getCircuitos().get(i).equals(circuito)){
                        cont++;
                    }
                }
                if(cont==0){

                    outPilotos.add(allPilotos.get(i));
                }
            }

            vista.dlmPilotosEnlace.removeAllElements();
            for (Pilotos piloto:outPilotos) {
                vista.dlmPilotosEnlace.addElement(piloto);
            }
            //actualizar(vista.dlmListPilotosCircuitos,"pilotosEnlace");



        }else
            **/

        if(e.getSource().equals(vista.listCircuitos)){
            if (vista.listCircuitos.getSelectedValue() !=null) {
                Circuitos circuito = (Circuitos) vista.listCircuitos.getSelectedValue();

                vista.txtNombreCircuito.setText(circuito.getNombre());
                vista.textLocalidadCircuito.setText(circuito.getLocalidad());
                vista.textFieldPistas.setText(String.valueOf(circuito.getCantidadPistas()));

                vista.dlmListPilotosCircuitos.removeAllElements();
                System.out.println(circuito.getPilotos().size());
                for (Pilotos pilo:circuito.getPilotos()) {
                    vista.dlmListPilotosCircuitos.addElement(pilo);
                }


            }
        } else if(e.getSource().equals(vista.listPilotos)){
            if (vista.listPilotos.getSelectedValue() !=null) {
                Pilotos pilotos = (Pilotos) vista.listPilotos.getSelectedValue();

                vista.textNombrePiloto.setText(pilotos.getNombre());
                vista.textTrofeosPiloto.setText(String.valueOf(pilotos.getTrofeos()));
                //Date input = pilotos.getProximaCarrera();

                //LocalDate date = LocalDate.of(input.getYear(), input.getMonth(), input.getDay());
                vista.txtFechaPiloto.setDate(pilotos.getProximaCarrera().toLocalDate());

                if (pilotos.getVehiculo() != null) {
                    vista.comboBoxVehiculoPiloto.setSelectedItem(pilotos.getVehiculo());
                }


            }
        }else if(e.getSource().equals(vista.listVehiculos)){
            if (vista.listVehiculos.getSelectedValue() !=null) {
                Vehiculos vehiculo = (Vehiculos) vista.listVehiculos.getSelectedValue();

                vista.textNombreVehiculo.setText(vehiculo.getNombre());
                vista.textPrecioVehiculo.setText(String.valueOf(vehiculo.getPrecio()));
                vista.txtMatriculaVehiculo.setText(vehiculo.getMatricula());


            }

        }else if(e.getSource().equals(vista.listMecanicos)){
            if (vista.listMecanicos.getSelectedValue() !=null) {
                Mecanicos mecanico = (Mecanicos) vista.listMecanicos.getSelectedValue();

                vista.textMecanicoNombre.setText(mecanico.getNombre());
                vista.textMecanicosExperiencia.setText(String.valueOf(mecanico.getAñosExperiencia()));
                vista.textMecanicosFormacion.setText(mecanico.getFormacion());
            }
        }

    }

    private void modificarMecanico(Mecanicos mecanico) {
        mecanico.setNombre(vista.textMecanicoNombre.getText());
        mecanico.setAñosExperiencia(Integer.parseInt(vista.textMecanicosExperiencia.getText()));
        mecanico.setFormacion(vista.textMecanicosFormacion.getText());


        if(mecanico.getVehiculo() != null) {
            mecanico.getVehiculo().removeMecanico(mecanico);
        }
        //Si he seleccionado un homicida en el cbox, anado a su victima
        Vehiculos vehiculoSeleccionado = (Vehiculos) vista.listaVehiculosEnMecanicos.getSelectedItem();
        if(vehiculoSeleccionado != null){
            vehiculoSeleccionado.addMecanico(mecanico);
        }



    }

    private void modificarPiloto(Pilotos piloto) {
        piloto.setNombre(vista.textNombrePiloto.getText());
        piloto.setTrofeos(Integer.parseInt(vista.textTrofeosPiloto.getText()));
        piloto.setProximaCarrera(Date.valueOf(vista.txtFechaPiloto.getDate()));


        if(piloto.getVehiculo() != null) {
            piloto.getVehiculo().removePiloto(piloto);
        }
        //Si he seleccionado un homicida en el cbox, anado a su victima
        Vehiculos vehiculoSeleccionado = (Vehiculos) vista.listaVehiculosEnPilotos.getSelectedItem();
        if(vehiculoSeleccionado != null){
            vehiculoSeleccionado.addPiloto(piloto);
        }



    }

}
