import usuarios.ListaUsuarios;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by DAM on 23/10/2018.
 */
//implementamos el actionListener al control de acceso
public class ControlAccesso implements ActionListener {
    private ListaUsuarios listaUsuarios;
    private VistaAccesso vistaAcceso;
    private Vista unavista;
    private Modelo unModelo;

    /**
     * @apiNote Constructor por el que le pasamos una lista de usuario y una vista de acceso
     *
     * @param listaUsuarios
     * @param vistaAccesso
     */
    public ControlAccesso(ListaUsuarios listaUsuarios, VistaAccesso vistaAccesso) {
        this.listaUsuarios=listaUsuarios;
        this.vistaAcceso=vistaAccesso;
        añadirListener();


    }

    private void añadirListener() {
        vistaAcceso.loginButton.addActionListener(this);
        vistaAcceso.registrerButton.addActionListener(this);
    }

    /**
     * @apiNote metodo overide del action listener para realizar cada accion
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            //Cogemos el login de la ventana de acceso y realizamos las comprobaciones en metodos
            case "login":
                comprobarUsuarios();
                break;
                //registramos un usuario en el array
            case "Registrar":
                añadirUsuario();

                break;

            default:
                break;
        }
    }

    /**
     * metodo para añadir usuarios al array
     */
    private void añadirUsuario() {
        String nombre = vistaAcceso.userText.getText();
        String contraseña = vistaAcceso.passwordText.getText();
        listaUsuarios.añadirUsuario(nombre,contraseña);
    }

    /**
     * metodo para comprobar los usuarios del array con el usuario y contraseña introducido por el cliente
     */
    private void comprobarUsuarios() {
            int contador=0;
            //Compruebo el usuario y la contraseña
            for (int i = 0; i < listaUsuarios.getListaUsuario().size(); i++) {
                //compruebo el usuario introducido y la contraseña con los usuarios del array
                if(listaUsuarios.getListaUsuario().get(i).getNombre().equals(vistaAcceso.userText.getText())){
                    if(listaUsuarios.getListaUsuario().get(i).getContrasena().equals(vistaAcceso.passwordText.getText())){
                        //genero la lista y el modelo
                        contador++;
                        unavista = new Vista();
                        unModelo = new Modelo();
                        //y genero el controlador al que le paso la vista y el modelo
                        Controlador uncontrolador = new Controlador(unavista, unModelo,vistaAcceso);
                        //escondo el frame de acceso
                        vistaAcceso.frame.setVisible(false);
                        break;
                        }else{

                        }
                    }else{

                }
                }
                //hago un control para indicar que no se ha encontrado usuario
                if(contador==0) {
                    vistaAcceso.ventanaUserNoEnContrado();
                }
            }

}
