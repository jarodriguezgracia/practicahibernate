import javax.swing.*;
import java.awt.*;

public class VistaAccesso {
	
	JTextField userText;
	JPasswordField passwordText;
	JButton loginButton;
	JFrame frame;
	JButton registrerButton;

	/**
	 * @apiNote inicializo la vista en el contructor y inicializo la vista
	 */
	public VistaAccesso(){
	frame = new JFrame("Control de acceso");
	frame.setSize(300, 150);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	JPanel panel = new JPanel();
	frame.add(panel);
	//llamo al metodo para introducir todo en el panel
	placeComponents(panel);
	frame.setResizable(false);

	frame.setVisible(true);
	}

	//Creamos el panel por posicionamiento en el metodo
	private void placeComponents(JPanel panel) {

		panel.setLayout(null);
	
		JLabel userLabel = new JLabel("User");
		userLabel.setBounds(10, 10, 80, 25);
		panel.add(userLabel);
	
		userText = new JTextField(20);
		userText.setBounds(100, 10, 160, 25);
		panel.add(userText);
	
		JLabel passwordLabel = new JLabel("Password");
		passwordLabel.setBounds(10, 40, 80, 25);
		panel.add(passwordLabel);
	
		passwordText = new JPasswordField(20);
		passwordText.setBounds(100, 40, 160, 25);
		panel.add(passwordText);
	
		loginButton = new JButton("login");
		loginButton.setBounds(10, 80, 80, 25);
		panel.add(loginButton);
		loginButton.setActionCommand("login");

		registrerButton = new JButton("Registrar");
		registrerButton.setBounds (100, 80, 160, 25);
		panel.add(registrerButton);
		registrerButton.setActionCommand("Registrar");
		
		
		//JButton registerButton = new JButton("register");
		//registerButton.setBounds(180, 80, 80, 25);
		//panel.add(registerButton);
	}

	/**
	 * metodo por si no se ha encontrado el usuario
	 */
	public void ventanaUserNoEnContrado(){
		//Creamos un joption pane para encontrar el usuario
		Frame pestana = new Frame();
		JPanel panel = new JPanel(new BorderLayout());
		JOptionPane.showMessageDialog(pestana,"No se ha encontrado usuario");
		
		
	}

}
