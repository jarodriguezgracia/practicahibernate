import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

public class Vista {
    JTabbedPane tabbedPane1;
    JTextField txtNombreCircuito;
    JTextField textLocalidadCircuito;
    JButton btAltaCircuito;
    JButton btnEliminarCircuito;
    JButton btModificarCircuito;
    JButton ConsultarCir;
    JList listPilotosEnlace;
    JList listCircuitosEnlace;
    JButton añadirPilotosHaCircuitoButton;
    JTextField textNombrePiloto;
    JTextField textTrofeosPiloto;
    JButton ButtonAltaPiloto;
    JButton ButtonEliminarPiloto;
    JButton buttonModificarPiloto;
    DatePicker txtFechaPiloto;
    JComboBox comboBoxVehiculoPiloto;
    JButton consultarCircuitosButton;
    JTextField textNombreVehiculo;
     JTextField textPrecioVehiculo;
     JButton ButtonAltaVehiculo;
     JButton ButtonEliminarVehiculo;
     JButton ButtonModificarVehiculo;
    JTextField txtMatriculaVehiculo;
    JButton accederButton;
    JButton añadiruser;
    JTextField textFieldPistas;
     JPanel panelPrincipal;
     JList listVehiculos;
     JTextField textMecanicoNombre;
     JTextField textMecanicosExperiencia;
     JTextField textMecanicosFormacion;

    JMenuItem opcionSalir;

     JList listMecanicos;
     DefaultListModel dlmMecanicos;

    DefaultListModel listaVehiculos;

     DefaultComboBoxModel listaVehiculosEnPilotos;

    JComboBox ComboBoxVehiculosMecanicos;
    JButton AltaMecanico;
    JButton EliminarMecanico;
    JButton ModificarMecanico;
     JList listPilotos;
     JList listCircuitos;
     JList listPilotosCircuitos;
    JButton eliminarPilotosDeCircuito;

    DefaultListModel dlmListPilotosCircuitos;
    DefaultListModel dlmCircuitos;

    DefaultListModel dlmPilotos;
    DefaultComboBoxModel listaVehiculosEnMecanicos;

    DefaultListModel dlmCircutiosEnlace;

    DefaultListModel dlmPilotosEnlace;
    JFrame frame;

    public Vista() {
         frame = new JFrame("Vista");
        frame.setContentPane(panelPrincipal);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        listaVehiculos = new DefaultListModel();
        listVehiculos.setModel(listaVehiculos);

        listaVehiculosEnPilotos = new DefaultComboBoxModel();
        comboBoxVehiculoPiloto.setModel(listaVehiculosEnPilotos);

        listaVehiculosEnMecanicos = new DefaultComboBoxModel();
        ComboBoxVehiculosMecanicos.setModel(listaVehiculosEnMecanicos);

        dlmMecanicos = new DefaultListModel();
        listMecanicos.setModel(dlmMecanicos);

        dlmPilotos = new DefaultListModel();
        listPilotos.setModel(dlmPilotos);

        dlmCircuitos = new DefaultListModel();
        listCircuitos.setModel(dlmCircuitos);

        dlmCircutiosEnlace = new DefaultListModel();
        listCircuitosEnlace.setModel(dlmCircutiosEnlace);

        dlmPilotosEnlace = new DefaultListModel();
        listPilotosEnlace.setModel(dlmPilotosEnlace);

        dlmListPilotosCircuitos = new DefaultListModel();
        listPilotosCircuitos.setModel(dlmListPilotosCircuitos);

        crearmenu();
        frame.pack();
        frame.setVisible(true);
    }

    private void crearmenu() {
        JMenuBar menuBar = new JMenuBar();

        JMenu menuArchivo = new JMenu("Archivo");



        opcionSalir = new JMenuItem("salir");

        opcionSalir.setActionCommand("Salir");








        menuArchivo.add(opcionSalir);

        menuBar.add(menuArchivo);

        frame.setJMenuBar(menuBar);
    }

    public void mensajeDeError(){
        JOptionPane.showMessageDialog(null, "Piloto ya añadido a circuito",
                "WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
    }

    public void mensajeDerelacion(){
        JOptionPane.showMessageDialog(null, "Elimine los pilotos del circuito antes de eliminar",
                "WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
    }
    public void mensajeSelecionCircuitoPiloto(){
        JOptionPane.showMessageDialog(null, "Selecciona circuitos y pilotos",
                "WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
    }

    public void mensajeNoHaySelecion(){
        JOptionPane.showMessageDialog(null, "Selecciona el objeto a eliminar",
                "WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
    }
}


