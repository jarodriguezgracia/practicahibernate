public class HiloUpdate extends Thread{
    Controlador controlador;
    Modelo modelo;
    Vista vista;
    int contador=0;

    public HiloUpdate(Vista vista, Modelo modelo, Controlador controlador) {
        this.controlador=controlador;
        this.modelo=modelo;
        this.vista=vista;
    }

    @Override
    public void run() {
        super.run();


        while (contador == 0){
            try {
                sleep(20000);
                System.out.println("llego "+ this);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(contador==0) {

                controlador.actualizar(vista.dlmMecanicos, "mecanicos");


                controlador.actualizar(vista.listaVehiculos, "vehiculos");

                controlador.actualizarComboxVehiculo(vista.listaVehiculosEnPilotos,"vehiculosPilotos");
                controlador.actualizarComboxVehiculo(vista.listaVehiculosEnMecanicos,"vehiculosMecanicos");

                controlador.actualizar(vista.dlmPilotos, "pilotos");
                controlador.actualizar(vista.dlmCircuitos, "circuitos");
                controlador. actualizar(vista.dlmCircutiosEnlace, "circuitosEnlace");
                //pilotosenlace
                controlador.actualizar(vista.dlmPilotosEnlace, "pilotos");
            }

        }
        System.out.println("fin hilo");


    }
    public void cambiarcontador(){
        contador=1;
    }
}
