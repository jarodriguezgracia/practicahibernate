package base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Circuitos {
    private int id;
    private String nombre;
    private String localidad;
    private int cantidadPistas;
    private List<Pilotos> pilotos;

    public Circuitos() {
        pilotos= new ArrayList<>();
    }

    public Circuitos(String nombre, String localidad, Integer cantidadPistas) {
        this.nombre = nombre;
        this.localidad = localidad;
        this.cantidadPistas = cantidadPistas;
        pilotos= new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Circuitos{" +
                "nombre='" + nombre + '\'' +
                ", localidad='" + localidad + '\'' +
                ", cantidadPistas=" + cantidadPistas +
                '}';
    }

    public String toString1() {
        return "Circuitos{" +
                "nombre='" + nombre + '\'' +
                ", localidad='" + localidad + '\'' +
                '}';
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre",unique = true)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "localidad")
    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    @Basic
    @Column(name = "cantidadPistas")
    public int getCantidadPistas() {
        return cantidadPistas;
    }

    public void setCantidadPistas(int cantidadPistas) {
        this.cantidadPistas = cantidadPistas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        base.Circuitos circuitos = (base.Circuitos) o;
        return id == circuitos.id &&
                cantidadPistas == circuitos.cantidadPistas &&
                Objects.equals(nombre, circuitos.nombre) &&
                Objects.equals(localidad, circuitos.localidad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, localidad, cantidadPistas);
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "pilotos_circuito", catalog = "", schema = "final", joinColumns = @JoinColumn(name = "id_circuito", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_piloto", referencedColumnName = "id", nullable = false))
    public List<Pilotos> getPilotos() {
        return pilotos;
    }

    public void setPilotos(List<Pilotos> pilotos) {
        this.pilotos = pilotos;
    }
}
