package base;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "pilotos_circuito", schema = "final", catalog = "")
@IdClass(PilotosCircuitoPK.class)
public class PilotosCircuito {
    private int idPiloto;
    private int idCircuito;

    @Id
    @Column(name = "id_piloto")
    public int getIdPiloto() {
        return idPiloto;
    }

    public void setIdPiloto(int idPiloto) {
        this.idPiloto = idPiloto;
    }

    @Id
    @Column(name = "id_circuito")
    public int getIdCircuito() {
        return idCircuito;
    }

    public void setIdCircuito(int idCircuito) {
        this.idCircuito = idCircuito;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PilotosCircuito that = (PilotosCircuito) o;
        return idPiloto == that.idPiloto &&
                idCircuito == that.idCircuito;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPiloto, idCircuito);
    }
}
