package base;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class PilotosCircuitoPK implements Serializable {
    private int idPiloto;
    private int idCircuito;

    @Column(name = "id_piloto")
    @Id
    public int getIdPiloto() {
        return idPiloto;
    }

    public void setIdPiloto(int idPiloto) {
        this.idPiloto = idPiloto;
    }

    @Column(name = "id_circuito")
    @Id
    public int getIdCircuito() {
        return idCircuito;
    }

    public void setIdCircuito(int idCircuito) {
        this.idCircuito = idCircuito;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PilotosCircuitoPK that = (PilotosCircuitoPK) o;
        return idPiloto == that.idPiloto &&
                idCircuito == that.idCircuito;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPiloto, idCircuito);
    }
}
