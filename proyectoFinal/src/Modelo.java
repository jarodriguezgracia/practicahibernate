import base.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import javax.persistence.Query;
import java.sql.Date;
import java.sql.SQLOutput;
import java.time.LocalDate;
import java.util.List;

public class Modelo {
    private SessionFactory factoria;
    private Session sesion;

    public Modelo(){

    }
    public void conectar() {
        /**
        Configuration configuration = new Configuration();

        configuration.configure();

        // Se registran las clases que hay que mapear con cada tabla de la base de datos
        configuration.addAnnotatedClass(Circuitos.class);
        configuration.addAnnotatedClass(Mecanicos.class);
        configuration.addAnnotatedClass(Pilotos.class);
        configuration.addAnnotatedClass(Vehiculos.class);



        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        factoria = configuration.buildSessionFactory(serviceRegistry);

        sesion = factoria.openSession();
         **/

         HibernateUtil.buildSessionFactory();
          HibernateUtil.openSession();
    }

    public void desconectar() {
        if (sesion != null){
            sesion.close();
    }

    }

    public void insertarVehiculo(String nombre, String precio, String matricula) {

       Double precioDouble = Double.parseDouble(precio);

       Vehiculos vehiculos = new Vehiculos(nombre,precioDouble,matricula);
        //eliminar linea
         Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        try{
        sesion.save(vehiculos);

        }catch (org.hibernate.exception.ConstraintViolationException er){
        sesion.clear();
        }
        sesion.getTransaction().commit();
        //  sesion.clear();

    }

    public List<Vehiculos> getVehiculos() {
//eliminar linea
          Session sesion = HibernateUtil.getCurrentSession();
        Query query = sesion.createQuery("FROM Vehiculos");
        List<Vehiculos> listaquery=  query.getResultList();
        //   sesion.clear();
        return listaquery;
    }

    public void insertarMecanico(String nombre, String añosExperiencia, String formacion, Vehiculos vehiculo) {

        Integer experiencia = Integer.parseInt(añosExperiencia);

        Mecanicos mecanico = new Mecanicos(nombre,experiencia,formacion,vehiculo);
//eliminar linea
         Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(mecanico);
        sesion.getTransaction().commit();
        //  sesion.clear();
    }

    public List<Mecanicos> getMecanicos() {
//eliminar linea
        Session sesion = HibernateUtil.getCurrentSession();
        Query query = sesion.createQuery("FROM Mecanicos");
        List<Mecanicos> listaquery=  query.getResultList();
        // sesion.clear();
        return listaquery;
    }
    public List<Pilotos> getPilotos() {
//eliminar linea
        Session sesion = HibernateUtil.getCurrentSession();
        Query query = sesion.createQuery("FROM Pilotos");
        List<Pilotos> listaquery=  query.getResultList();
        //   sesion.clear();
        return listaquery;
    }

    public List<Circuitos> getCircuitos() {
//eliminar linea
          Session sesion = HibernateUtil.getCurrentSession();
        Query query = sesion.createQuery("FROM Circuitos");
        List<Circuitos> listaquery=  query.getResultList();
        //  sesion.clear();
        return listaquery;
    }

    public void insertarPilotos(String nombre, int trofeos, LocalDate fecha, Vehiculos vehiculo) {

        Date fechaGood = Date.valueOf(fecha);
        Pilotos pilotos = new Pilotos(nombre,trofeos,fechaGood,vehiculo);
//eliminar linea
         Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();

        sesion.save(pilotos);

        sesion.getTransaction().commit();
        //  sesion.clear();
        /**
        if(sesion.isOpen()){
            System.out.println("estoy conectado");
            sesion.close();
        }
         **/

//        for (Pilotos piloto:vehiculo.getPilotos()) {
 //           System.out.println(piloto.toString());
  //      }
    }

    public void insertarCircuitos(String nombre, String localidad, String cantidadPistas) {

    int cpistas = Integer.parseInt(cantidadPistas);

    Circuitos circuito = new Circuitos(nombre,localidad,cpistas);
//eliminar linea
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        try {


        sesion.save(circuito);
        }catch (org.hibernate.exception.ConstraintViolationException er){
            sesion.clear();
        }
        sesion.getTransaction().commit();
        //   sesion.clear();
    }
    public void actualizarCircuito(Circuitos circuito) {
//eliminar linea
          Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(circuito);
        sesion.getTransaction().commit();
      //   sesion.clear();
    }

    public void actualizarPiloto(Pilotos piloto) {
//eliminar linea
           Session sesion = HibernateUtil.getCurrentSession();

        sesion.beginTransaction();
        sesion.saveOrUpdate(piloto);
        sesion.getTransaction().commit();
      //   sesion.clear();
    }
    public void actualizarMecanico(Mecanicos mecanicos) {

//eliminar linea
         Session sesion = HibernateUtil.getCurrentSession();

        sesion.beginTransaction();
        sesion.saveOrUpdate(mecanicos);
        sesion.getTransaction().commit();
      //   sesion.clear();
    }
    public void actualizarVehiculo(Vehiculos vehiculos) {
//eliminar linea
         Session sesion = HibernateUtil.getCurrentSession();

        sesion.beginTransaction();
        sesion.saveOrUpdate(vehiculos);
        sesion.getTransaction().commit();
      //   sesion.clear();

    }


    public void eliminarCircuito(Circuitos ciruitos) {
        //eliminar linea
        Session sesion = HibernateUtil.getCurrentSession();

        sesion.beginTransaction();
        sesion.delete(ciruitos);
        sesion.getTransaction().commit();
       //  sesion.clear();
    }

    public void eliminarPiloto(Pilotos piloto) throws Exception {
        //eliminar linea
        Session sesion = HibernateUtil.getCurrentSession();

        sesion.beginTransaction();
        sesion.delete(piloto);
        sesion.getTransaction().commit();
        sesion.clear();
    }

    public void eliminarVehiculo(Vehiculos vehiculo) {

        //eliminar linea
         Session sesion = HibernateUtil.getCurrentSession();

        sesion.beginTransaction();
        sesion.delete(vehiculo);
        sesion.getTransaction().commit();
        // sesion.clear();
    }

    public void eliminarMecanico(Mecanicos mecanico) {
        //eliminar linea
         Session sesion = HibernateUtil.getCurrentSession();

        sesion.beginTransaction();
        sesion.delete(mecanico);
        sesion.getTransaction().commit();
        //sesion.clear();
    }

    public void insertarPilotosEnCircuito(Pilotos piloto, Circuitos circuito)throws Exception {

        //eliminar linea
         Session sesion = HibernateUtil.getCurrentSession();

        sesion.beginTransaction();
            circuito.getPilotos().add(piloto);
            piloto.getCircuitos().add(circuito);
        sesion.getTransaction().commit();
         sesion.clear();




    }


    public void eliminarRelacion(Circuitos circuito, Pilotos piloto) {

        Session sesion = HibernateUtil.getCurrentSession();

        sesion.beginTransaction();
        circuito.getPilotos().remove(piloto);
        piloto.getCircuitos().remove(circuito);
        sesion.getTransaction().commit();
        sesion.clear();

        for (Pilotos pl:circuito.getPilotos()) {
            System.out.println(pl.toString());
        }
        for (Circuitos pl:piloto.getCircuitos()) {
            System.out.println(pl.toString());
        }
    }
}
