package base;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Mecanicos {
    private int id;
    private String nombre;
    private int añosExperiencia;
    private String formacion;
    private Vehiculos vehiculo;

    public Mecanicos() {

    }

    public Mecanicos(String nombre, Integer añosExperiencia, String formacion, Vehiculos vehiculo) {
        this.nombre = nombre;
        this.añosExperiencia = añosExperiencia;
        this.formacion = formacion;
        this.vehiculo = vehiculo;
    }

    @Override
    public String toString() {
        return "Mecanicos{" +
                "nombre='" + nombre + '\'' +
                ", añosExperiencia=" + añosExperiencia +
                ", formacion='" + formacion + '\'' +
                ", vehiculo=" + vehiculo +
                '}';
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "añosExperiencia")
    public int getAñosExperiencia() {
        return añosExperiencia;
    }

    public void setAñosExperiencia(int añosExperiencia) {
        this.añosExperiencia = añosExperiencia;
    }

    @Basic
    @Column(name = "formacion")
    public String getFormacion() {
        return formacion;
    }

    public void setFormacion(String formacion) {
        this.formacion = formacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mecanicos mecanicos = (Mecanicos) o;
        return id == mecanicos.id &&
                añosExperiencia == mecanicos.añosExperiencia &&
                Objects.equals(nombre, mecanicos.nombre) &&
                Objects.equals(formacion, mecanicos.formacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, añosExperiencia, formacion);
    }

    @ManyToOne
    @JoinColumn(name = "vehiculo", referencedColumnName = "id")
    public Vehiculos getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculos vehiculo) {
        this.vehiculo = vehiculo;
    }
}
