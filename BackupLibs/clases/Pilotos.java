package base;

import javax.persistence.*;
import java.sql.Date;
import java.util.*;

@Entity
public class Pilotos {
    private int id;
    private String nombre;
    private int trofeos;
    private Date proximaCarrera;
    private Vehiculos vehiculo;
    private List<Circuitos> circuitos;


    public Pilotos() {
        circuitos = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Pilotos{" +
                "nombre='" + nombre + '\'' +
                ", trofeos=" + trofeos +
                ", proximaCarrera=" + proximaCarrera +
                ", vehiculo=" + vehiculo +
                '}';
    }

    public String toString1() {
        return "Pilotos{" +
                "nombre='" + nombre + '\'' +
                ", trofeos=" + trofeos +
                '}';
    }

    public Pilotos(String nombre, Integer trofeos, Date proximaCarrera, Vehiculos vehiculo) {
        this.nombre = nombre;
        this.trofeos = trofeos;
        this.proximaCarrera = proximaCarrera;
        this.vehiculo = vehiculo;
        circuitos = new ArrayList<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "trofeos")
    public int getTrofeos() {
        return trofeos;
    }

    public void setTrofeos(int trofeos) {
        this.trofeos = trofeos;
    }

    @Basic
    @Column(name = "proxima_carrera")
    public Date getProximaCarrera() {
        return proximaCarrera;
    }

    public void setProximaCarrera(Date proximaCarrera) {
        this.proximaCarrera = proximaCarrera;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pilotos pilotos = (Pilotos) o;
        return id == pilotos.id &&
                trofeos == pilotos.trofeos &&
                Objects.equals(nombre, pilotos.nombre) &&
                Objects.equals(proximaCarrera, pilotos.proximaCarrera);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, trofeos, proximaCarrera);
    }

    @ManyToOne()
    @JoinColumn(name = "vehiculo", referencedColumnName = "id")
    public Vehiculos getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculos vehiculo) {
        this.vehiculo = vehiculo;
    }

    @ManyToMany(mappedBy = "pilotos")
    public List<Circuitos> getCircuitos() {
        return circuitos;
    }

    public void setCircuitos(List<Circuitos> circuitos) {
        this.circuitos = circuitos;
    }
}
