package base;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Vehiculos {
    private int id;
    private String nombre;
    private double precio;
    private String matricula;
    private List<Mecanicos> mecanicos;
    private List<Pilotos> pilotos;

    public Vehiculos(String nombre, Double precio, String matricula) {
        this.nombre = nombre;
        this.precio = precio;
        this.matricula = matricula;
        mecanicos= new ArrayList<>();
        pilotos= new ArrayList<>();

        System.out.println(pilotos.size());
    }
    public Vehiculos(){
        mecanicos= new ArrayList<>();
        pilotos= new ArrayList<>();
        System.out.println(pilotos.size());
    }

    public void removeMecanico(Mecanicos mecanicos){
        mecanicos.setVehiculo(null);
        if(this.mecanicos.contains(mecanicos)) {
            this.mecanicos.remove(mecanicos);
        }
    }

    public void addMecanico(Mecanicos mecanicos){
        this.mecanicos.add(mecanicos);
        mecanicos.setVehiculo(this);
    }
    public void removePiloto(Pilotos piloto){
        piloto.setVehiculo(null);
        if(this.pilotos.contains(piloto)) {
            this.pilotos.remove(piloto);
        }
    }

    public void addPiloto(Pilotos piloto){
        System.out.println(this.pilotos.size());
        this.pilotos.add(piloto);
        piloto.setVehiculo(this);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "matricula",unique = true)
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehiculos vehiculos = (Vehiculos) o;
        return id == vehiculos.id &&
                Double.compare(vehiculos.precio, precio) == 0 &&
                Objects.equals(nombre, vehiculos.nombre) &&
                Objects.equals(matricula, vehiculos.matricula);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, precio, matricula);
    }

    @OneToMany(mappedBy = "vehiculo")
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<Mecanicos> getMecanicos() {
        return mecanicos;
    }

    public void setMecanicos(List<Mecanicos> mecanicos) {
        this.mecanicos = mecanicos;
    }

    @OneToMany(mappedBy = "vehiculo")
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<Pilotos> getPilotos() {
        return pilotos;
    }

    public void setPilotos(List<Pilotos> pilotos) {
        this.pilotos = pilotos;
    }

    @Override
    public String toString() {
        return "Vehiculos{" +
                "nombre='" + nombre + '\'' +
                ", precio=" + precio +
                ", matricula='" + matricula + '\'' +
                '}';
    }
}
